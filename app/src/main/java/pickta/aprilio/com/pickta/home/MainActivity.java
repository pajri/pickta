package pickta.aprilio.com.pickta.home;

        import android.content.Intent;
        import android.graphics.Typeface;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.TextView;

        import pickta.aprilio.com.pickta.R;
        import pickta.aprilio.com.pickta.editing.EditingActivity;
        import pickta.aprilio.com.pickta.settings.SettingsActivity;
        import pickta.aprilio.com.pickta.take_pictures.TakePicturesActivity;
        import pickta.aprilio.com.pickta.utils.AppConstants;

public class MainActivity extends AppCompatActivity{
    //region Attributes
    private TextView txvAppName;
    //endregion
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupViews();
    }

    /**
     * Handle on click button
     * @param view clicked button
     */
    public void onClick(View view){
        switch(view.getId()){
            case R.id.imbTakePictures :
                Intent intTakePictures = new Intent(this, TakePicturesActivity.class);
                startActivity(intTakePictures);
                break;
            case  R.id.imbSettings :
                Intent intSettings = new Intent(this, SettingsActivity.class);
                startActivity(intSettings);
                break;
        }
    }


    /**
     * Intiialize view attributes
     */
    private void setupViews(){
        /*START set app name*/
        txvAppName = (TextView) findViewById(R.id.txvAppName);
        Typeface fontLSansUni = Typeface.createFromAsset(getAssets(), AppConstants.Assets.FONTS_DIR+"/lsansuni.ttf"); //get font
        txvAppName.setTypeface(fontLSansUni);
        /*END set app name*/
    }
}
