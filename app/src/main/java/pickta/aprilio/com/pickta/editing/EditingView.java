package pickta.aprilio.com.pickta.editing;

import android.graphics.Bitmap;

/**
 * Created by Aprilio Pajri on 03-Dec-17.
 */

public interface EditingView {
    void showImage(Bitmap image);
    void showErrorMessage(String message);
}
