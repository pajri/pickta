package pickta.aprilio.com.pickta.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;

/**
 * Created by Aprilio Pajri on 03-Dec-17.
 */

public class FileUtils {

    public static File getPictureDirectory(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Pickta");

        return mediaStorageDir;
    }

    public static Bitmap loadImage(String filePath){
        File file = new File(filePath);
        if(file.exists()){
            Bitmap image = BitmapFactory.decodeFile(file.getAbsolutePath());
            return image;
        }else{
            return null;
        }
    }
}
