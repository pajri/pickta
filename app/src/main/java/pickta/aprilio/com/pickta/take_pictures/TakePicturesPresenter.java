package pickta.aprilio.com.pickta.take_pictures;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Environment;
import android.os.Process;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pickta.aprilio.com.pickta.utils.FileUtils;

/**
 * Created by Aprilio Pajri on 01-Oct-17.
 */

public class TakePicturesPresenter {
    //region Attributes
    private String TAG = "TakePicturesPresenter";
    private TakePicturesView takePicturesView;
    private List<Thread> pictureTakenThreadList; //TODO handle shared variable
    //endregion

    //region Costructors
    TakePicturesPresenter(TakePicturesView takePicturesView){
        this.takePicturesView = takePicturesView;
    }
    //endregion

    void setupCamera(){
        //TODO add camera 2 for > lollipop
        //TODO get settings
        takePicturesView.setupCamera(null);
    }

    void onPictureTaken(byte[] imageStream, String fileName, int cameraRotation){
        if(pictureTakenThreadList == null){
            pictureTakenThreadList = new ArrayList<>();
        }

        PictureTakenRunnable pictureTakenRunnable = new PictureTakenRunnable(imageStream, fileName, cameraRotation);
        Thread thread = new Thread(pictureTakenRunnable);
        thread.start();
    }

    void onPictureTakenFinished(){
        takePicturesView.showProgressDialog();
        boolean isAllFinished;
        do{
            isAllFinished = true;
            for (Thread process :
                    pictureTakenThreadList) {
                isAllFinished = isAllFinished && (process.getState() == Thread.State.TERMINATED);
                if(!isAllFinished) break;
            }
        }while(!isAllFinished);

        pictureTakenThreadList.clear();
        pictureTakenThreadList = null;
        takePicturesView.hideProgressDialog();

        takePicturesView.goToEditActivity();

    }

    void onDestroy(){
        takePicturesView=null;
    }

    private File getOutputMediaFile(String imageFileName){
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_UNMOUNTED)){
            String directoryType = Environment.DIRECTORY_PICTURES;
            File mediaStorageDir = FileUtils.getPictureDirectory();

            if(!mediaStorageDir.exists()){
                //create dir
                boolean isCreated = mediaStorageDir.mkdir();
                if(!isCreated){
                    //create directory failed
                    Log.d(TAG,"in getOutputMediaFile : Unable to create directory "+mediaStorageDir.getPath());
                    return null;
                }
            }

            //create media file name
            File mediaFile;
            String filePath = mediaStorageDir.getPath() + File.separator + imageFileName;
            mediaFile = new File(filePath);
            return mediaFile;
        }else{
            Log.d(TAG,"in getOutputMediaFile : Storage device is not mounted");
        }

        return null;
    }

    //region Inner Class
    private class PictureTakenRunnable implements Runnable{
        private final String TAG = "PictureTakenRunnable";
        private byte[] data;
        private String imageFileName;
        private int cameraRotation;

        public PictureTakenRunnable(byte[] data, String imageFileName, int cameraRotation){
            this.data = data;
            this.imageFileName = imageFileName;
            this.cameraRotation=cameraRotation;
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            pictureTakenThreadList.add(Thread.currentThread());

            File pictureFile = getOutputMediaFile(imageFileName);
            Log.d(this.TAG,"in run : Saving picture started : "+pictureFile.getName());
            if (pictureFile == null){
                Log.d(this.TAG, "in run : Error creating media file, check storage permissions.");
            }else{
                try{
                    FileOutputStream fileOutputStream = new FileOutputStream(pictureFile);
                    Bitmap image = BitmapFactory.decodeByteArray(data,0,data.length); //convert to bitmap to rotate

                    //rotate image
                    Matrix rotation = new Matrix();
                    rotation.postRotate(cameraRotation);
                    image = Bitmap.createBitmap(image,0,0,image.getWidth(),image.getHeight(),rotation,true);

                    //convert back to byte array
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

                    fileOutputStream.write(byteArrayOutputStream.toByteArray());
                    fileOutputStream.close();
                } catch (FileNotFoundException e) {
                    Log.d(this.TAG, "in run : File not found: " + e.getMessage());
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.d(this.TAG, "in run : Error accessing file: " + e.getMessage());
                    e.printStackTrace();
                } catch (Exception e){
                    Log.d(this.TAG,"in run : "+e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }
    //endregion
}
