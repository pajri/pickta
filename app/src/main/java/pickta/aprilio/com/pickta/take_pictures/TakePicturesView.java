package pickta.aprilio.com.pickta.take_pictures;

import android.content.Context;
import android.widget.FrameLayout;

import java.util.Map;

/**
 * Created by Aprilio Pajri on 01-Oct-17.
 */

public interface TakePicturesView {
    //Context getAppContext();
    //FrameLayout getFramePreview();
    void setupCamera(Map<String,String> cameraSettings);
    void showProgressDialog();
    void hideProgressDialog();
    void goToEditActivity();
}
