package pickta.aprilio.com.pickta.helper;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.io.IOException;

/**
 * Created by Aprilio Pajri on 01-Oct-17.
 */

public class CameraHelper {
    //region Attributes
    private final String TAG = CameraHelper.class.getName();
    private Camera camera = null;
    private CameraPreview cameraPreview;
    private OrientationEventListener orientationEventListener;
    private Context context;
    //endregion

    //region Constructors
    public CameraHelper(Context context){
        this.context = context;
    }
    //endregion

    public void setCamera(Camera camera){
        this.camera = camera;
    }

    public boolean takePicture(Camera.ShutterCallback shutterCallback, Camera.PictureCallback raw, Camera.PictureCallback jpeg){
        if(camera != null){
            camera.takePicture(shutterCallback, raw, jpeg);
            return true;
        }else{
            return false;
        }
    }

    public void createCameraPreview(WindowManager windowManager){
        setupOrientationEventListner(windowManager);
        cameraPreview = new CameraPreview(this.context);
    }

    private void setupOrientationEventListner(final WindowManager windowManager){
        if(orientationEventListener == null){
            orientationEventListener = new OrientationEventListener(this.context) {
                @Override
                public void onOrientationChanged(int orientation) {
                    if(orientation != OrientationEventListener.ORIENTATION_UNKNOWN &&
                            camera != null){
                        int rotation = windowManager.getDefaultDisplay().getRotation(); //get screen rotation
                        int degree = (rotation*90+90) - (rotation%2)*180;
                        camera.setDisplayOrientation(degree);
                    }
                }
            };
        }
    }

    //region get set
    public Camera getCamera(){
        return this.camera;
    }
    public CameraPreview getCameraPreview(){return this.cameraPreview;}
    //endregion

    //region Inner Class
    private class CameraPreview extends SurfaceView implements SurfaceHolder.Callback{
        private SurfaceHolder surfaceHolder;
        public CameraPreview(Context context) {
            super(context);

            surfaceHolder = getHolder();
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS); //deprecated but required in android < 3.0. but are we using andorid 3.0? :D
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            //tell the camera where to display the preview
            try {
                orientationEventListener.enable();
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (IOException e) {
                Log.e("MainActivity","Error on surfaceCreated "+e.getMessage());
                e.printStackTrace();
            }

        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Log.d("MainActivity","On surface changed");
            if(surfaceHolder.getSurface()==null){
                //preview surface does not exist
                return;
            }

            try {
                camera.stopPreview();
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            }catch(Exception ex){
                Log.e("MainActivity","Error while changing preview");
                ex.printStackTrace();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            orientationEventListener.disable();
            surfaceHolder=null;
        }
    }
    //endregion

}
