package pickta.aprilio.com.pickta.take_pictures;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.hardware.Camera;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import pickta.aprilio.com.pickta.R;
import pickta.aprilio.com.pickta.editing.EditingActivity;
import pickta.aprilio.com.pickta.utils.AppConstants;

public class TakePicturesActivity extends AppCompatActivity implements TakePicturesView, Camera.PictureCallback {
    //region Attributes
    private TextView txvImageCount;
    private FrameLayout frmPreview;

    private TakePicturesPresenter takePicturesPresenter;
    private int maxPictureTaken=3;
    private int pictureTaken;
    private String timeStampFirstCapture; //timestamp of first captured photo

    private final int REQUEST_CAMERA = 0;
    private final String TAG = this.getClass().getName();

    private Camera camera;
    private OrientationEventListener orientationEventListener;
    private CameraPreview cameraPreview;
    private ProgressBar prgWaitTakePictureFinished;
    private int cameraRotation=0;


    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_take_pictures);

        if(takePicturesPresenter == null){
            takePicturesPresenter = new TakePicturesPresenter(this);
        }

        //request permission
        if(android.os.Build.VERSION.SDK_INT >= 23){
            int permissionCameraCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            int permissionWriteExternalStorageCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if(permissionCameraCheck != PackageManager.PERMISSION_GRANTED
                    || permissionWriteExternalStorageCheck != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},REQUEST_CAMERA);
            }else{
                activityOnCreate();
            }
        }else{
            activityOnCreate();
        }
    }

    @Override
    protected void onDestroy() {
        takePicturesPresenter.onDestroy();
        takePicturesPresenter=null;
        super.onDestroy();
    }

    private void activityOnCreate(){
        setupView();
        takePicturesPresenter.setupCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(grantResults.length>0){
            switch (requestCode){
                case REQUEST_CAMERA:
                    if(grantResults[0]==PackageManager.PERMISSION_GRANTED
                            && grantResults[1]==PackageManager.PERMISSION_GRANTED){
                        activityOnCreate();
                    }else{
                        try {
                            PermissionInfo infoCameraPermission = getPackageManager()
                                    .getPermissionInfo(Manifest.permission.CAMERA, PackageManager.GET_META_DATA);
                            PermissionInfo infoWriteExternalStorageInfo = getPackageManager()
                                    .getPermissionInfo(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.GET_META_DATA);
                            new AlertDialog.Builder(this)
                                    .setTitle(getString(R.string.title_error))
                                    .setMessage(getString(R.string.message_permission_required,
                                            infoCameraPermission.name+" and "+infoWriteExternalStorageInfo.name))
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    });
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }

                    }
                    break;
            }
        }
    }

    public void setupCamera(Map<String,String> cameraSettings){
        camera = getCameraInstance();
        orientationEventListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int orientation) {
                if(orientation != OrientationEventListener.ORIENTATION_UNKNOWN &&
                        camera != null){
                    int rotation = getWindowManager().getDefaultDisplay().getRotation(); //get screen rotation
                    cameraRotation = (rotation*90+90) - (rotation%2)*180;
                    camera.setDisplayOrientation(cameraRotation);

                }
            }
        };

        cameraPreview = new CameraPreview(this);
        frmPreview.addView(cameraPreview);
    }

    public void onClick(View view) {
        switch(view.getId()){
            case R.id.fabCapture :
                pictureTaken=0;
                timeStampFirstCapture = new SimpleDateFormat("yyyyMMdd_hhmmss", Locale.US).format(new Date());
                txvImageCount.setVisibility(View.VISIBLE);
                camera.takePicture(null, null, this);
                break;
        }
    }

    private Camera getCameraInstance(){
        Camera camera = null;
        try {
            camera = Camera.open();
        }catch(Exception e){
            Log.e(TAG,e.getMessage());
            e.printStackTrace();
        }

        return camera;
    }

    private void setupView(){
        frmPreview = (FrameLayout) findViewById(R.id.frmPreview);
        txvImageCount = (TextView) findViewById(R.id.txv_picture_count);
        prgWaitTakePictureFinished = (ProgressBar) findViewById(R.id.prg_wait_take_picture_finished);
    }

    @Override
    public void showProgressDialog(){
        prgWaitTakePictureFinished.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressDialog(){
        prgWaitTakePictureFinished.setVisibility(View.GONE);
    }

    @Override
    public void goToEditActivity() {
        Intent intentEditing = new Intent(this,EditingActivity.class);
        intentEditing.putExtra(EditingActivity.KEY_TIMESTAMP,timeStampFirstCapture);
        startActivity(intentEditing);
    }

    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        String fileName = String.format(AppConstants.ImageConstant.imageFileNameFormat,timeStampFirstCapture,pictureTaken);
        pictureTaken++;
        takePicturesPresenter.onPictureTaken(data,fileName, cameraRotation);
        txvImageCount.setText(getResources().getQuantityString(R.plurals.txv_picture_taken,pictureTaken,pictureTaken));
        this.camera.startPreview();
        if(pictureTaken < maxPictureTaken){
            this.camera.takePicture(null,null,this);
        }
        else{
            //take picture ended
            pictureTaken=0;
            txvImageCount.setVisibility(View.GONE);
            takePicturesPresenter.onPictureTakenFinished();
        }
    }

    //region Inner Class
    private class CameraPreview extends SurfaceView implements SurfaceHolder.Callback{
        private SurfaceHolder surfaceHolder;
        public CameraPreview(Context context) {
            super(context);

            surfaceHolder = getHolder();
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS); //deprecated but required in android < 3.0. but are we using andorid 3.0? :D
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            //tell the camera where to display the preview
            try {
                orientationEventListener.enable();
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            } catch (IOException e) {
                Log.e(TAG,"Error on surfaceCreated "+e.getMessage());
                e.printStackTrace();
            }

        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Log.d(TAG,"On surface changed");
            if(surfaceHolder.getSurface()==null){
                //preview surface does not exist
                return;
            }

            try {
                camera.stopPreview();
                camera.setPreviewDisplay(holder);
                camera.startPreview();
            }catch(Exception ex){
                Log.e(TAG,"Error while changing preview");
                ex.printStackTrace();
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            orientationEventListener.disable();
            surfaceHolder=null;
        }
    }
    //endregion
}
