package pickta.aprilio.com.pickta.utils;

import java.io.File;

/**
 * Created by Aprilio Pajri on 21-Sep-17.
 */

public class AppConstants {
    public static class Assets{
        public static final String FONTS_DIR = "fonts";
    }

    public static class ImageConstant{
        /*
        1 : file name
        2 : increment number
         */
        public static final String imageFileNameFormat = "%1$s_%2$s.jpg";

        /*
        1 : Image directory
        2 : Image filename
         */
        public static final String imageFilePathFormat = "%1$s"+ File.separator+"%2$s";
    }

    public static class Method{
        public static final String[] METHODS = {"Mean","Median","Mode"};
        public static final int MEAN = 0;
        public static final int MEDIAN = 1;
        public static final int MODE = 2;
    }
}
