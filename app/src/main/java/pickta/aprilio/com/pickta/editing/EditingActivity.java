package pickta.aprilio.com.pickta.editing;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.github.chrisbanes.photoview.PhotoView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import pickta.aprilio.com.pickta.R;
import pickta.aprilio.com.pickta.utils.AppConstants;
import pickta.aprilio.com.pickta.utils.CodeUtils;

public class EditingActivity extends AppCompatActivity implements EditingView {

    //region
    public final static String KEY_TIMESTAMP = "KEY_TIMESTAMP";
    private final String TAG = EditingActivity.class.getName();

    private RelativeLayout relativeBottomEditView;
    private ImageView imgShowHideEditView;
    private PhotoView photovEditing;
    private Spinner spnMethod;

    private boolean isEditViewUp;
    private EditingPresenter editingPresenter;
    private String imageFileName;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editing);
        setupView();

        editingPresenter = new EditingPresenter(this);

        Intent intent = getIntent();
        imageFileName = intent.getStringExtra(KEY_TIMESTAMP);
        try {
            editingPresenter.loadImage(imageFileName);
        } catch (IOException e) {
            CodeUtils.showErrorMessage(getResources().getString(R.string.mesasge_error_action_fail,
                    getResources().getString(R.string.action_load_image)),this);
            Log.e(TAG,e.getMessage());
            e.printStackTrace();
        }
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.img_showhide_view:
                if(isEditViewUp){
                    float translate = relativeBottomEditView.getHeight();
                    relativeBottomEditView.animate().translationY(translate);
                    imgShowHideEditView.animate().translationY(translate);
                    imgShowHideEditView.setImageResource(R.drawable.ic_keyboard_arrow_up);
                }else{
                    relativeBottomEditView.animate().translationY(0);
                    imgShowHideEditView.animate().translationY(0);
                    imgShowHideEditView.setImageResource(R.drawable.ic_keyboard_arrow_down);
                }
                isEditViewUp = !isEditViewUp;
                break;
        }
    }

    private void setupView(){
        relativeBottomEditView = (RelativeLayout) findViewById(R.id.relative_bottom_edit_view);
        isEditViewUp = true;
        imgShowHideEditView = (ImageView) findViewById(R.id.img_showhide_view);

        spnMethod = (Spinner) findViewById(R.id.spn_method);
        //initialize values of spinner
        ArrayList<String> spinnerItems = new ArrayList<String>(Arrays.asList(AppConstants.Method.METHODS));
        spinnerItems.add(0,"None");
        ArrayAdapter<String> methodAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                spinnerItems);
        spnMethod.setAdapter(methodAdapter);

        spnMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try{
                    String selectedMethod = (String) spnMethod.getItemAtPosition(position);
                    editingPresenter.processEditing(imageFileName,selectedMethod);
                }catch(Exception exception){
                    String action = getResources().getString(R.string.action_edit_image);
                    CodeUtils.showErrorMessage(getResources().getString(R.string.mesasge_error_action_fail, action),getApplicationContext());

                    Log.e(TAG, exception.getMessage());
                    exception.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        photovEditing = (PhotoView) findViewById(R.id.photov_editing);

    }


    @Override
    public void showImage(Bitmap image) {
        photovEditing.setImageBitmap(image);
    }

    @Override
    public void showErrorMessage(String message) {
        CodeUtils.showErrorMessage(message,this);
    }
}
