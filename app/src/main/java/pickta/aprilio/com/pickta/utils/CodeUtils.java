package pickta.aprilio.com.pickta.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;

import pickta.aprilio.com.pickta.R;

/**
 * Created by Aprilio Pajri on 13-Jan-18.
 */

public class CodeUtils {

    /**
     * message with title parameter.
     * @param title String error dialog title
     * @param message String error message
     * @param context Context context
     */
    public static void showErrorMessage(String title, String message, Context context){
        AlertDialog.Builder errorDialog = new AlertDialog.Builder(context);
        errorDialog.setTitle(title);
        errorDialog.setMessage(message);
        errorDialog.setTitle(context.getResources().getString(R.string.dialog_error_title));
        errorDialog.setPositiveButton("Ok",null);
    }

    /**
     * Error message without title parameter. Use default title from string resource.
     * @param message String error message
     * @param context Context context
     */
    public static void showErrorMessage(String message, Context context){
        AlertDialog.Builder errorDialog = new AlertDialog.Builder(context);
        errorDialog.setMessage(message);
        errorDialog.setTitle(context.getResources().getString(R.string.dialog_error_title));
        errorDialog.setPositiveButton("Ok",null);
    }
}
