package pickta.aprilio.com.pickta.editing;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pickta.aprilio.com.pickta.utils.AppConstants;
import pickta.aprilio.com.pickta.utils.CodeUtils;
import pickta.aprilio.com.pickta.utils.FileUtils;

/**
 * Created by Aprilio Pajri on 03-Dec-17.
 */

public class EditingPresenter {
    //TODO read opencv documentation for android development :
    // https://docs.opencv.org/2.4/doc/tutorials/introduction/android_binary_package/dev_with_OCV_on_Android.html#dev-with-ocv-on-android
    private EditingView editingView;

    public EditingPresenter(EditingView editingView){
        this.editingView = editingView;
    }

    void processEditing(String imageFileName, String method){
        //get all images
        List<Bitmap> imageList = new ArrayList<>();

        String pictureDirectoryPath = FileUtils.getPictureDirectory().getPath();
        int imageCount = 0;
        String fileName = String.format(AppConstants.ImageConstant.imageFileNameFormat,imageFileName,imageCount);
        Bitmap image = FileUtils.loadImage(String.format(AppConstants.ImageConstant.imageFilePathFormat,pictureDirectoryPath,fileName));
        while(image != null){
            imageList.add(image);

            imageCount++;
            fileName = String.format(AppConstants.ImageConstant.imageFileNameFormat,imageFileName,imageCount);
            image = FileUtils.loadImage(String.format(AppConstants.ImageConstant.imageFilePathFormat,pictureDirectoryPath,fileName));
        }

        //process edit image
        if(method.equals(AppConstants.Method.METHODS[AppConstants.Method.MEAN])){
            processMean(imageList);
        }else if(method.equals(AppConstants.Method.METHODS[AppConstants.Method.MEDIAN])){
            processMedian(imageList);
        }else if(method.equals(AppConstants.Method.METHODS[AppConstants.Method.MODE])){
            processModus(imageList);
        }
    }

    private Bitmap processMean(List<Bitmap> imageList){
        Bitmap bitmap = Bitmap.createBitmap(imageList.get(0).getWidth(), imageList.get(0).getHeight(), imageList.get(0).getConfig());
        int meanR,meanG,meanB;
        List<int[]> pixelList = new ArrayList<>();
        int pixelSize = bitmap.getHeight()*bitmap.getWidth();
        for (Bitmap image :
                imageList) {
            int[] pixels = new int[pixelSize];
            image.getPixels(pixels,0,image.getWidth(),0,0,image.getWidth(),image.getHeight());
            pixelList.add(pixels);
        }

        int numberOfImage = imageList.size();
        int[] result = new int[pixelSize];
        for (int i = 0; i < pixelSize; i++) {
            meanR=0;meanG=0;meanB=0;
            for (int[] pixels:
                        pixelList){
                int colorInts = pixels[i];
                meanR += Color.red(colorInts);
                meanG += Color.green(colorInts);
                meanB += Color.blue(colorInts);
            }
            meanR /= numberOfImage;
            meanG /= numberOfImage;
            meanB /= numberOfImage;
            result[i] = Color.rgb(meanR,meanG,meanB);
        }

        bitmap.setPixels(result,0,bitmap.getWidth(),0,0,bitmap.getWidth(),bitmap.getHeight());
//        for (int i = 0; i < bitmap.getWidth(); i++) {
//            for (int j = 0; j < bitmap.getHeight(); j++) {
//                meanR = 0; meanG=0; meanB=0;
//                for (Bitmap image :
//                        imageList) {
//                    int colorInts = image.getPixel(i,j);
//                    meanR += Color.red(colorInts);
//                    meanG += Color.green(colorInts);
//                    meanB += Color.blue(colorInts);
//                }
//                meanR /= numberOfImage;
//                meanG /= numberOfImage;
//                meanB /= numberOfImage;
//                bitmap.setPixel(i,j,Color.rgb(meanR,meanG,meanB));
//            }
//        }
        return bitmap;
    }

    private Bitmap processMedian(List<Bitmap> imageList){
        Bitmap bitmap = Bitmap.createBitmap(imageList.get(0).getWidth(), imageList.get(0).getHeight(), imageList.get(0).getConfig());
        int numberOfImage = imageList.size();
        int medianR,medianG,medianB;
        for (int i = 0; i < bitmap.getWidth(); i++) {
            for (int j = 0; j < bitmap.getHeight(); j++) {
                int[] arrR = new int[numberOfImage];
                int[] arrG = new int[numberOfImage];
                int[] arrB = new int[numberOfImage];

                int k=0;
                for (Bitmap image :
                        imageList) {
                    arrR[k]=Color.red(image.getPixel(i,j));
                    arrG[k]=Color.green(image.getPixel(i,j));
                    arrB[k]=Color.blue(image.getPixel(i,j));
                }
                Arrays.sort(arrR);
                Arrays.sort(arrG);
                Arrays.sort(arrB);

                int medianIndex1 = (arrR.length + 1) / 2;
                int medianIndex2 = ((arrR.length + 1) / 2) + ((arrR.length+1)%2);

                medianR = (arrR[medianIndex1] + arrR[medianIndex2])/2;
                medianG = (arrG[medianIndex1] + arrG[medianIndex2])/2;
                medianB = (arrB[medianIndex1] + arrB[medianIndex2])/2;

                bitmap.setPixel(i,j,Color.rgb(medianR,medianG,medianB));
            }
        }
        return bitmap;
    }

    private Bitmap processModus(List<Bitmap> imageList){
        Bitmap bitmap = Bitmap.createBitmap(imageList.get(0).getWidth(), imageList.get(0).getHeight(), imageList.get(0).getConfig());
        int modeR,modeG,modeB;

        for (int i = 0; i < bitmap.getWidth(); i++) {
            for (int j = 0; j < bitmap.getHeight(); j++) {
                int[] arrModeR = new int[255];
                int[] arrModeG = new int[255];
                int[] arrModeB = new int[255];

                for (Bitmap image :
                        imageList) {
                    arrModeR[Color.red(image.getPixel(i,j))]+=1;
                    arrModeG[Color.green(image.getPixel(i,j))]+=1;
                    arrModeB[Color.blue(image.getPixel(i,j))]+=1;
                }
                Arrays.sort(arrModeR);
                Arrays.sort(arrModeG);
                Arrays.sort(arrModeB);

                modeR = arrModeR[254];
                modeG = arrModeG[254];
                modeB = arrModeB[254];

                bitmap.setPixel(i,j,Color.rgb(modeR,modeG,modeB));
            }
        }
        return bitmap;
    }

    public void loadImage(String fileName) throws IOException {
        String name = String.format(AppConstants.ImageConstant.imageFileNameFormat,fileName,"0"); //first file name
        String dir = FileUtils.getPictureDirectory().getPath();
        String path = String.format(AppConstants.ImageConstant.imageFilePathFormat,dir,name);
        File file = new File(path);
        if(file.exists()){
            //get orientation with exif
            ExifInterface exifInterface = new ExifInterface(file.getPath());
            int rotation = 0;
            switch(exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,-1)){
                case ExifInterface.ORIENTATION_ROTATE_90 :
                    rotation=90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180 :
                    rotation=180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270 :
                    rotation=270;
                    break;
            }

            Bitmap image = BitmapFactory.decodeFile(file.getPath());
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            image = Bitmap.createBitmap(image,0,0,image.getWidth(),image.getHeight(),matrix,true);

            editingView.showImage(image);
        }else{
            editingView.showErrorMessage("Unable to load image.");
        }
    }
}
